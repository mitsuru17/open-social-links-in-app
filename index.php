<?php require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;
define('IS_MOBILE', $detect->isMobile()); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Open social links in app - Noémie Kerroux</title>
	<link rel="stylesheet" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<header>
		<div class="title">Noémie Kerroux</div>
		<div class="subtitle">Web &amp; mobile developer</div>
	</header>
	<div class="page-title">
		<h1>Social Network Links<br> <span>Open on app</span></h1>
	</div>
	<div class="container">
		<?php $networks_list = [
		   [
		     "name" => "Facebook",
		     "class" => "facebook",
		     "web_url" => "https://www.facebook.com/FacebookFrance/",
		     "mobile_url" => "fb://profile/179106818789009"
		   ],
		   [
		     "name" => "Twitter",
		     "class" => "twitter",
		     "web_url" => "https://twitter.com/twitter",
		     "mobile_url" => "twitter:///user?screen_name=twitter"
		   ],
		   [
		     "name" => "Instagram",
		     "class" => "instagram",
		     "web_url" => "https://www.instagram.com/instagram/",
		     "mobile_url" => "instagram://user?username=instagram"
		   ],
		   [
		     "name" => "LinkedIn",
		     "class" => "linkedIn",
		     "web_url" => "https://fr.linkedin.com/company/linkedin",
		     "mobile_url" => "https://fr.linkedin.com/company/linkedin"
		   ],
		   [
		     "name" => "Youtube",
		     "class" => "youtube",
		     "web_url" => "https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ",
		     "mobile_url" => "https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ"
		   ],
		 ]; ?>
			<?php foreach ($networks_list as $key => $network): ?>
				<a class="<?php echo IS_MOBILE ? 'mobile-redirect' : ''; ?> <?php echo $network['class']; ?>" href="<?php echo IS_MOBILE ? $network['mobile_url'] : $network['web_url']; ?>" target="_blank" data-backup="<?php echo $network['web_url']; ?>">
					<?php echo $network['name']; ?>
				</a>
			<?php endforeach ?>
			<a href="https://gitlab.com/noemie-kerroux/open-social-links-in-app" class="none gitlab" target="_blank">Open on Gitlab</a>
			<a href="http://noemiekerroux.fr" target="_blank" class="none nk-website">noemiekerroux.fr</a>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function(){
            jQuery(".mobile-redirect").click(function(event){
              event.preventDefault();
              var href = jQuery(this).attr("href");
              var backup = jQuery(this).data("backup");
              window.location = href;
              setTimeout(function () {
              	var res = confirm("Open in the browser ?");
              	if(res == true) {
              		window.location = backup;
              	}
              }, 1000);
            });
        });
	 </script>
</body>
</html>